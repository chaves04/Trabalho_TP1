# <strong>[MANUAL](MANUAL)<strong>

## <strong>[BOUNCING BALL PARTE INICIAL](BOUNCING BALL PARTE INICIAL)

### <strong>OBJETIVOS

O objetivo desse projeto é gerar o caminho (coordenadas) que a bola irá percorrer dentro dos eixos (x,y) do plano cartesiano e gerar o gráfico referente
a posição da bola com as 10.000 coordenadas geradas pelo programa.


### <strong> COMO GERAR O EXECUTÁVEL:
Para compilar o programa e gerar os 10.000 coordenadas é necessário entrar no caminho do projeto e executar pelo terminal Linux usando o Makefile e digitando o seguinte comando:<br>
```make test-ball```<br>
OBS.: Por padrão ele gera o executável com o nome ```[a.exe](a.exe)```<br>

### <strong>COMO GERAR O ARQUIVO TEXTO:
Para gerar o arquivo texto com as coordenadas é necessário executar no Bash do Windows o comando:<br>
```a > teste1.txt```<br>
[ArquivoCoordenadas(x,y).txt](/Coordenadas.txt)

### <strong>GERARANDO GRÁFICO
Para gerar o gráfico com as coordenadas da bola é necessário exportar as coordenadas do arquivo texto criado para duas colunas do Microsoft Excel 2016.<br>
Após importar as coordenadas ele irá colocar as duas coordenadas em uma única coluna, assim necessário separar as duas coordenadas em duas colunas.<br>
A sequência de passos para separar em colunas é:<br>
Selecione a coluna  "A" -> Dados -> Texto para Colunas -> "Delimitado" -> Selecionar o delimitador "espaços" -> Concluir<br>
Para inserir o gráfico:<br>
Selecione as colunas "A" e "B" -> "Inserir" -> "Inserir gráfico de dispersão (x,y) -> "Dispersão com linhas suaves"<br>
Para visualizar melhor:<br>
Eixo vertical: Selecione o eixo vertical -> "Opções do eixo" -> "Rótulos" -> "Posição do Rótulo" -> Selecione "Inferior"<br>
Eixo horizontal: Selecione o eixo horizontal -> "Opções do eixo" -> "Rótulos" -> "Posição do Rótulo" -> Selecione "Inferior"<br>

![gráfico](/img/COORDENADAS.PNG)

## <strong>[BOUNCING BALL PARTE FINAL](BOUNCING BALL PARTE FINAL)

#### <strong>OBJETIVO
Gerar um gráfico usando as cordenadas do objetivo1 para visualizarmos as coordenadas x e y, nome do arquvi Baucing Ball.<br>
![gráfico](/img/Bouncing.png)<br>

#### <strong>ARQUIVOS
<strong>graphics.cpp:</strong> Define os métodos das classes Drawable (Abstrata) e Figure (hrerdeira de Drawable), e define a função run.<br>
<strong>ball.cpp:</strong> Arquivo que contém a implementação das funções e construtor do objeto "Ball"<br>
<strong>test-ball-graphics.cpp:</strong> Declara e define os métodos da classe BallDrawable (herdeira de Ball e Drawable), e define uma main, essa, chama os comandos que mostram a simulação na tela.<br>

### <strong> COMO GERAR O EXECUTÁVEL:
Usando o Makefila com o script já determinado executamos usando o comando:
```make test-ball-graphics```<br>

## <strong>[SPRING-MASS](SPRING-MASS)

#### <strong>OBJETIVO
 O objetivo do spring-mass é o de gerar as coordenadas, junto com o tamanho da mola e a energia total do sistema, para no futuro gerar um gráfico com esses dados.<br>
[Saida.txt](/x.txt)

#### <strong>ARQUIVOS
<strong>springmass.cpp:</strong> Define os métodos das classes Mass, Spring e SpringMass.<br>
<strong>springmass.h:</strong> Declara as classes Vector2, Mass, Spring e SpringMass, e também declara e define os operadores +, -, * e \** de **Vector2.<br>
<strong>test-springmass.cpp:</strong> Declara e define uma main roda a simulação e a imprime na linha de comando.<br>

#### <strong> COMO GERAR O EXECUTÁVEL:
Usando o Makefila com o script já determinado executamos usando o comando:<br>
```make test-springmass```<br>
Para criar o gráfico usei a ferramenta <strong>octave</strong>, precisei instalar, pois não vem nativo no linux. 
Para gerar o plot, precisei criar um novo arquivo no formato que o octave aceitava: ![plot_springmass.m](plot_springmass.m).<br> 
Por não conseguir usar o octave, acabei tendo que usar o excel para gerar o gráfico.<br>
<strong>Erro do octave:</strong> <br> ![Erro_octave.png](/img/Erro_octave.png)

#### <strong> GERANDO GRÁFICO

Usando os dados gerados pelo springmass, consegui gerar um gráfico usando o Execel, mostrando o movimento das massas.<br>
![gráfico](/img/Plot_Massas.jpeg)<br>

## <strong>[GRAPHICS](GRAPHICS)

#### <strong>OBJETIVO
O objetivo é entender os conceitos utilizados, de forma aplicada, além de criar construtores que podem receber diversos tipos de argumentos.
Deve-se notar que a partir dos métodos criados pode-se modificar a velocidade, peso, altura inicial ou número de iterações (quantas coordenadas serão geradas) da bola.

#### <strong>ARQUIVOS
<strong>graphics.cpp:</strong> Define os métodos das classes Drawable (Abstrata) e Figure (herdeira de Drawable), e define a função run.<br>
<strong>springmass.cpp:</strong> Define os métodos das classes Mass, Spring e SpringMass.<br>
<strong>test-springmass-graphics.cpp:</strong> Declara e define a classe SpringMassDrawable e uma main que roda a simulação.<br>

#### <strong> COMO GERAR O EXECUTÁVEL:

Usando o Makefila com o script já determinado executamos usando o comando:<br>
```make test-springmass-graphics```<br>
![gráfico](/img/Plot_2Massas.png)<br>


###### <strong>[REQUISITOS](REQUISITOS)

<strong>Sistema operacional: <strong>Ubuntu 16.04 LTS </strong><br>
<strong>Compilador: <strong>Terminal Linux</strong><br>
<strong>Compilador: <strong>make g++ (tdm64-1)</strong><br>
<strong>Versão: <strong>5.1.0</strong><br>
<strong>Bibliotecas: <strong>```#include <iostream>```</strong><br>
<strong>SOFTWARES:<br>
<strong>IDE de desenvolvimento: <strong>Code::Blocks</strong><br>
<strong>Gerar gráfico: <strong>Excel 2016</strong><br>

###### <strong>[TODOS OS ARQUIVOS USADOS:](TODOS OS ARQUIVOS USADOS:)

<strong>ball.h:</strong> Arquivo que contém Classe "Ball" e suas assinaturas de funções e atributos<br>
<strong>ball.cpp:</strong> Arquivo que contém a implementação das funções e construtor do objeto "Ball"<br>
<strong>test-ball.cpp:</strong> Arquivo que contém a "main" que são chamadas as funções<br>
<strong>Makefile:</strong> Contém os comandos de compilação dos programas do projéto.<br>
<strong>graphics.cpp:</strong> Define os métodos das classes Drawable (Abstrata) e Figure (herdeira de Drawable), e define a função run.<br>
<strong>graphics.h:</strong> Declara a função run e as classes Drawable e Figure.<br>
<strong>plot-ball.R:</strong> Um script da lingugem R para plotar o gráfico da posição da Bola durante a simulação.<br> 
<strong>simulation.h:</strong> Declara a classe abstrata Simulation, ela serve de base para a classe Ball.<br>
<strong>springmass.cpp:</strong> Define os métodos das classes Mass, Spring e SpringMass.<br>
<strong>springmass.h:</strong> Declara as classes Vector2, Mass, Spring e SpringMass, e também declara e define os operadores.<br>
<strong>test-ball-graphics.cpp:</strong> Declara e define os métodos da classe BallDrawable (herdeira de Ball e Drawable), e define uma main, essa, chama os comandos que mostram a simulação na tela.<br>
<strong>test-springmass-graphics.cpp:</strong> Declara e define a classe SpringMassDrawable e uma main que roda a simulação.<br>
<strong>test-springmass.cpp:</strong> Declara e define uma main roda a simulação e a imprime na linha de comando.<br>
